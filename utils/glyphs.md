
###### useful unicode characters, NERD fonts symbols
```bash
# arrows etc: ➤ ⮡ ⮠ ✗ 🗴 ✓ ✔             ﰲ
# arrows up/down: ﰬ ﰵ      
# hardware:   力 曆 歷 轢 年 憐 戀 撚             
# OSes, software:               
# indicators:            直睊               ﱝ
# batteries:                                               
# folders:        ﱮ
# dices:           
```
